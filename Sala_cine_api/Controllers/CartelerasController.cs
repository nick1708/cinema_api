﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sala_cine_api.Models;

namespace Sala_cine_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartelerasController : ControllerBase
    {
        private readonly sala_cineContext _context;

        public CartelerasController(sala_cineContext context)
        {
            _context = context;
        }

        // GET: api/Carteleras
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cartelera>>> GetCartelera()
        {
            return await _context.Cartelera.ToListAsync();
        }

        // GET: api/Carteleras/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Cartelera>> GetCartelera(int id)
        {
            var cartelera = await _context.Cartelera.FindAsync(id);

            if (cartelera == null)
            {
                return NotFound();
            }

            return cartelera;
        }

        // PUT: api/Carteleras/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCartelera(int id, Cartelera cartelera)
        {
            if (id != cartelera.Id)
            {
                return BadRequest();
            }

            _context.Entry(cartelera).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarteleraExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Carteleras
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Cartelera>> PostCartelera(Cartelera cartelera)
        {
            _context.Cartelera.Add(cartelera);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCartelera", new { id = cartelera.Id }, cartelera);
        }

        // DELETE: api/Carteleras/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Cartelera>> DeleteCartelera(int id)
        {
            var cartelera = await _context.Cartelera.FindAsync(id);
            if (cartelera == null)
            {
                return NotFound();
            }

            _context.Cartelera.Remove(cartelera);
            await _context.SaveChangesAsync();

            return cartelera;
        }

        private bool CarteleraExists(int id)
        {
            return _context.Cartelera.Any(e => e.Id == id);
        }
    }
}
