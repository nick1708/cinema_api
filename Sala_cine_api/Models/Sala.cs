﻿using System;
using System.Collections.Generic;

namespace Sala_cine_api.Models
{
    public partial class Sala
    {
        public Sala()
        {
            Cartelera = new HashSet<Cartelera>();
            Ticket = new HashSet<Ticket>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int NumeroAsientos { get; set; }

        public virtual ICollection<Cartelera> Cartelera { get; set; }
        public virtual ICollection<Ticket> Ticket { get; set; }
    }
}
