﻿using System;
using System.Collections.Generic;

namespace Sala_cine_api.Models
{
    public partial class Actor
    {
        public Actor()
        {
            Pelicula = new HashSet<Pelicula>();
        }

        public int Id { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public virtual ICollection<Pelicula> Pelicula { get; set; }
    }
}
