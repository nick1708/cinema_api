﻿using System;
using System.Collections.Generic;

namespace Sala_cine_api.Models
{
    public partial class Ticket
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public double Costo { get; set; }
        public int Existencias { get; set; }
        public int IdPelicula { get; set; }
        public int IdSala { get; set; }

        public virtual Pelicula IdPeliculaNavigation { get; set; }
        public virtual Sala IdSalaNavigation { get; set; }
    }
}
