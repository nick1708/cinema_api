﻿using System;
using System.Collections.Generic;

namespace Sala_cine_api.Models
{
    public partial class Categorias
    {
        public Categorias()
        {
            Pelicula = new HashSet<Pelicula>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<Pelicula> Pelicula { get; set; }
    }
}
