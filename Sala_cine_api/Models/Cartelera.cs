﻿using System;
using System.Collections.Generic;

namespace Sala_cine_api.Models
{
    public partial class Cartelera
    {
        public int Id { get; set; }
        public int IdPelicula { get; set; }
        public int IdSala { get; set; }
        public DateTime Horarios { get; set; }

        public virtual Pelicula IdPeliculaNavigation { get; set; }
        public virtual Sala IdSalaNavigation { get; set; }
    }
}
