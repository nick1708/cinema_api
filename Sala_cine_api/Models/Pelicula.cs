﻿using System;
using System.Collections.Generic;

namespace Sala_cine_api.Models
{
    public partial class Pelicula
    {
        public Pelicula()
        {
            Cartelera = new HashSet<Cartelera>();
            Ticket = new HashSet<Ticket>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int IdActor { get; set; }
        public int IdCategoria { get; set; }

        public virtual Actor IdActorNavigation { get; set; }
        public virtual Categorias IdCategoriaNavigation { get; set; }
        public virtual ICollection<Cartelera> Cartelera { get; set; }
        public virtual ICollection<Ticket> Ticket { get; set; }
    }
}
